"""
Fortune Cookie Service
"""

import os
import subprocess
from flask import Flask, request, Response, jsonify
from flask.templating import render_template
from subprocess import run, PIPE
from random import randrange

app = Flask(__name__, static_url_path='', static_folder='static',)
version = '1.0-python-unsecure'

# https://stackoverflow.com/a/62394945
hostname = subprocess.check_output('hostname').decode('utf8')


def get_fortune():
    number = randrange(1000)
    fortune = run('fortune', stdout=PIPE, text=True).stdout
    return number, fortune


@app.route("/", methods=['GET', 'POST'])
def fortune():
    """
    Print a random, hopefully interesting, adage
    """
    number, fortune = get_fortune()
    if request.method == 'GET':
        name = request.args.get('name')
    elif request.method == 'POST':
        name = request.form['name']
    mimetype = request.mimetype
    if mimetype == 'application/json':
        resp = jsonify(
            {'number': number, 'fortune': fortune, 'version': version, 'name': name, 'hostname': hostname})
        resp.headers['X-Fortune-Version'] = version
        return resp

    if mimetype == 'text/plain':
        result = f'''Fortune {version} cookie of the day #{str(number)} for {name}:

{fortune}
Pod: {hostname}'''
        resp = Response(result, mimetype='text/plain')
        resp.headers['X-Fortune-Version'] = version
        return resp

    # In all other cases, respond with HTML
    # This template is named "fortune.unsafe" because files with .html
    # extension are automatically escaped by Jinja to prevent XSS injection!
    # https://blog.nvisium.com/injecting-flask
    html = render_template('fortune.unsafe', number=number,
                           fortune=fortune, version=version, hostname=hostname, name=name)
    resp = Response(html, mimetype='text/html')
    resp.headers['X-Fortune-Version'] = version
    return resp


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=os.environ.get('listenport', 8080))
